\section{Memory consolidation \textit{VS.} memory performance isolation}
\label{sec_problem}
In order to demonstrate the above issues experimentally, we run a reference experiment, with only one container that executes with two CPU cores and with limits not set.
The container executes an OLTP benchmark with a \SI{4}{\giga\byte} database.
Next, we run the experiment with two containers, first with limits not set, then with limits set.
In all cases, we use as workload a standard benchmark (\texttt{sysbench oltp}) \cite{alexey_kopytov_sysbench_nodate}.
Throughput is measured from the output; memory size and disk activity are gathered by \texttt{docker}.

\subsection{Determining reference level}
\label{subsec_problem_reference}
The reference experiment, measures the baseline levels of throughput, memory footprint and disk activity of a single container without competing workload.
We define throughput as the number of transaction that the benchmark is able to process in one second.
A transaction is a set of SQL requests that read a database.
The highest measured throughput in this case is 2000 transactions per second (t/s).
The maximum memory footprint and maximum disk inputs are measured at \SI{2.8}{\giga\byte} and 100 reads per second respectively.
These values are pictured in Figures \ref{fig_default} to \ref{fig_mechanism} as dashed black horizontal lines.
In our experimental scenario (described in the next section), we define ``high'' throughput as answering 2000 t/s (\textit{i.e.} the maximum throughput).
We arbitrarily set ``low'' throughput to 10\% percent of high throughput, \textit{i.e.}, 200 transactions per second.
Low throughput is depicted as horizontal dotted black line in Figure \ref{fig_default} to Figure \ref{fig_mechanism}.
We also define ``intermediate'' throughput as 1500 t/s.
The horizontal colored dashed lines in Figure \ref{fig_transactions} to Figure \ref{fig_transactions_mechanism} correspond to the Service Level Objective (SLO), \textit{i.e.} a container is satisfied if its throughput equals or exceeds its SLO \cite{beyer_site_2016}.

\subsection{Experimental scenario}
\label{subsec_problem_scenario}
We now describe our multi-container experiment, showing that memory performance isolation does not occur without limits being set, and that memory consolidation is thwarted by the \texttt{max} and \texttt{soft} limits.
It features two concurrent containers, A and B, executing the OLTP workload and experiences changes in activity.
To test satisfaction, we define container A to be satisfied if it processes 1700 t/s and B 1100 t/s.
When both containers are offered high load, we expect to observe memory performance isolation.
When one of the containers receives a high load and the other receives low load or is stopped, we expect memory consolidation to let the former reach the maximum throughput.
Specifically, the experiment goes through six phases, as follows:
\begin{enumerate}[$\varphi_1$]
	\item\label{step_1} A and B are both offered high load.
	Memory pressure will occur, because physical memory is less than the combined size of the databases.
	The size of the page cache decreases, causing an increase in physical disk I/O, hence a decrease in throughput.
	\item\label{step_2} A continues to receive high load, but it decreases for B.
	If memory consolidation is effective, A should be able to take memory from B, and increase its throughput.
	\item\label{step_3} B receives high load and A intermediate load.
	This step is similar to \step{{\ref{step_1}}}.
	We expect both containers to have low throughput.
	\item\label{step_4} Both containers receive low load.
	Containers should be able to answer all the transactions.
	\item\label{step_5} B has intermediate load, not A.
	This step mirrors \step{{\ref{step_2}}}.
	\item\label{step_6} A stops completely.
	B still receive high throughput.
	It should be able to reach maximum performance.
\end{enumerate}
Each phase lasts 180 seconds.

\subsection{Experimental environment}
\label{subsec_problem_environment}
Our experimental machine is part of the Grid'5000 testbed \cite{grid5000}.
It has two Intel\textregistered Xeon\textregistered Gold 6130 CPU clocked at \SI{2.1}{\giga\hertz}, with \SI{192}{\giga\byte} of DDR4 memory and an SSD \cite{intel_intel_2017}.
To create memory pressure, we artificially limit memory size, by running the experiments within a VM restricted to 4 CPU cores and \SI{3}{\giga\byte} of memory.

We use \texttt{qemu 3.1.0} as the VM hypervisor, \texttt{docker 19.03.2}  and its \texttt{python} library \texttt{docker-py 4.0.2} to manage containers \cite{qemu_qemu_2019, docker_docker_nodate, noauthor_docker_py_nodate}.
Our benchmark is \texttt{sysbench oltp} \cite{alexey_kopytov_sysbench_nodate}.
We modified the benchmark code in order to be able to vary the throughput, as described previously.
The benchmark reads requests from a \texttt{mysql 5.7} database \cite{noauthor_mysql_nodate}.
We run our experiments on Linux 4.19 \cite{greg_kroah_hartman_linux_2018}.

Our two containers (A and B) run with two cores each.
Each one reads a database of \SI{4}{\giga\byte}.

We run the scenario described in Section \ref{subsec_problem_scenario} ten times and compute the mean and standard deviation of throughput, memory and disk inputs every second, plotted in Figure \ref{fig_default} to Figure \ref{fig_mechanism}.

\input{TEX/figure.tex}

\subsection{Experiment with no limits set}
\label{subsec_problem_without}
The goal of this first experiment is to show that, with limits unset, all containers are reclaimed equally, and there is no memory performance isolation.
Results of the scenario of Section \ref{subsec_problem_scenario} are depicted in Figure \ref{fig_default}.
Figure \ref{fig_transactions} plots throughputs in the different phases, Figure \ref{fig_memory} plots memory footprints and Figure \ref{fig_disk} disk inputs.

In \step{1}, both containers receive high load.
Figure \ref{fig_transactions} shows that they have the same throughput.
This can be explained by looking at \step{1} in Figure \ref{fig_memory}, because the containers have the same memory allocation.
There is no memory performance isolation.

In \step{2}, throughput of the container receiving high load increases at the expense of one with low load.
This phase, in Figure \ref{fig_memory}, shows that the memory footprint of the container with high load increases, and the container with low throughput load.
This memory increase results in a larger I/O cache, as visible in Figure \ref{fig_disk}.
Memory consolidation is taking place; however, it is imperfect, since an active container never reaches the maximum performance nor is satisfied.

During \step{6}, as A stops, B's memory allocation increases, and consequently it does fewer reads from the disk, increasing its performance to the maximum level.

The throughput of container B spikes at the transition between \step{3} and \step{4}.
This shows that B is overloaded in \step{3}, and excess transactions are queued; they are served in \step{4} where B has more resources available.
This behavior is visible in Figure \ref{fig_transactions} to Figure \ref{fig_transactions_mechanism}.

To sum up, with limits not set, memory consolidation occurs imperfectly and there is no memory performance isolation.

\subsection{Experiment setting \texttt{max} limit}
\label{subsec_problem_max}
This experiment aims to show that setting the \texttt{max} limit thwarts memory consolidation.
We run the same experiment as above, now setting the \texttt{max} limit to \SI{1.8}{\giga\byte} and \SI{1}{\giga\byte} for A and B respectively.
These values were chosen such that their total equals the maximum memory footprint.

Overall, Figure \ref{fig_transactions_max} shows that A performs better than B.
Container A will be able to serve more transactions in \step{1} and \step{3} in Figure \ref{fig_transactions_max}, compared to Figure \ref{fig_transactions}.
This is explained by Figure \ref{fig_memory_max}, showing that memory footprints follow their \texttt{max} limits (depicted as dashed lines).
A has more memory than B, and will do less disk I/O (as shown in Figure \ref{fig_disk_max}).

Figure \ref{fig_memory_max} shows that \texttt{max} limit hinders memory consolidation, in \step{2}, when B was offered low load A's footprint does not grow; it is not able to reach maximum or be satisfied.
So, \texttt{max} limits ensures memory performance isolation.
Furthermore, there is no memory consolidation in \step{6}: when A stops, B cannot increase its memory footprint past its \texttt{max} limit and thus experiences only a small performance increase non reaching the maximum level.

In summary, \texttt{max} limit supports memory performance isolation (since A acquires more memory than B) but memory consolidation is impeded.

\subsection{Experiment setting \texttt{soft} limit}
\label{subsec_problem_soft}
The \texttt{soft} limit, similarly to \texttt{max} limit, thwarts memory consolidation.
We show this by running the same experiment, setting \texttt{soft} limits instead of \texttt{max} limits.

In Figure \ref{fig_soft}, the numbers are very similar to Figure \ref{fig_max}.
This behavior can easily be explained since there is memory pressure in \step{1} to \step{5}.
So containers footprints will be pushed under their \texttt{soft} limits which have the same value as the previous \texttt{max} limits.
As a result, \texttt{soft} limit guarantees memory performance isolation when there is memory pressure.

The only difference is in \step{6}, where Figure \ref{fig_memory_soft}, shows that B's footprint grows compared to Figure \ref{fig_memory_max}.
Indeed, since A was stopped, there is no memory pressure in \step{6}, and the \texttt{soft} limit mechanism is not activated.
This enables B to increase its memory footprint and to reach the maximum performance.

In summary, the \texttt{soft} limit mechanism also supports memory performance isolation (B has less memory than A) under memory pressure.
This limits memory consolidation but when memory pressure disappears, memory consolidation becomes effective again.

\subsection{Summary}
\label{subsec_problem_summary}
% Check avec Marc.
We conclude that the existing mechanisms are insufficient as they do not ensure memory consolidation and memory performance isolation.
When the limits are not set, memory consolidation occurs, but imperfectly and there is no memory performance isolation.
The \texttt{max} limit ensures memory performance isolation, but thwarts memory consolidation.
The \texttt{soft} limit is similar under memory pressure, but allows memory consolidation when memory abounds.
Table \ref{table_summary} summarizes these results.

\begin{table}
	\centering

	\begin{tabular}{l|c|c}
		\hline
% 		\diagbox{}{Characteristics}
		Mechanism used & {\scriptsize Memory consolidation} & {\scriptsize Memory performance isolation}\\
		\hline
		limits not set & weak & no\\
		\texttt{max} limit & no & yes\\
		\texttt{soft} limit & no & yes\\
	\end{tabular}

	\caption{Summary of memory consolidation and memory performance isolation of existing mechanisms in Linux}
	\label{table_summary}
\end{table}