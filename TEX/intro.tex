\section{Introduction}
\label{sec_intro}
Infrastructure as a Service (IaaS) lets multiple clients rent hardware to execute their services.
It is standard practice in IaaS to \emph{consolidate} several logical servers on the same physical machine, thus amortizing cost \cite{zhang_virtualization_2012}. % Trouver des citations pour Amazon, Alibaba, OVH et Azure.
However, there is a conflict between server consolidation, and allocating sufficient resources to each client that it reaches satisfactory performance.
The execution of one logical server should not disturb the others: the logical servers should remain \emph{isolated} from one another.

To ensure both consolidation and isolation, a common approach is to use Virtual Machines (VM).
Unfortunately, VM are heavyweight and waste resources.
A recent alternative is the ``containers'', a group of processes with sharing and isolation properties \cite{amazon_amazon_nodate, microsoft_microsoft_nodate, alibaba_alibaba_nodate, ovh_ovh_nodate}.
Containers support security (\textit{e.g.}, a file in one container cannot be accessed by another), ease of deployment (\textit{e.g.}, it is possible to start a container with a simple shell command) and fine-grain resource control (\textit{e.g.}, a container can be pinned to a specific CPU core) \cite{docker_inc_what_nodate}.

To ensure \emph{memory performance isolation}, \textit{i.e.}, guaranteeing to each container enough memory for it to perform well, administrator limits the total amount of physical memory that a container may use at the expense of others.
If it exceeds its limits, some of its memory will be reclaimed, making it available to others.
The Linux kernel will reclaim pages from the file page cache first, resulting in a performance decrease for containers that perform I/O \cite{the_kernel_development_community_concepts_nodate}.

In previous work, we showed that these limits impede memory consolidation \cite{laniel_highlighting_2019}.
Moreover, the limits to container size are static, and do not adapt to the containers' dynamic behavior.
This is a problem, because it is hard to estimate the amount of memory needed by an application to be satisfied \cite{gregg_working_2018, nitu_working_2018}.
Furthermore, the metrics available to the kernel to evaluate its policies (\textit{e.g.}, frequency of page faults, I/O requests, use of CPU cycles, \textit{etc.}) are not helpful to gauge whether performance is satisfactory.
They are not directly relevant to performance as experienced from the application perspective, which is better characterized by, for instance, response time or throughput measured at application level.

To solve these problems, we propose a new approach, called the Memory Optimization Light\footnote{Our solution uses the colors of traffic lights to indicate container performance.} (MemOpLight).
It is based on application-level feedback from containers.
Our mechanism aims to rebalance memory allocation in favor of unsatisfied containers, while not penalizing the satisfied ones.
By doing so, we guarantee application satisfaction, while consolidating memory; this also improves overall resource consumption.

Our main contributions are the following:
\begin{inparaenum}[(i)]
	\item An experimental demonstration of the limitations of the existing Linux mechanisms.
	\item The design of a simple feedback mechanism from application to the kernel.
	\item An algorithm for adapting container memory allocation.
	\item And implementation in Linux and experimental confirmation.
\end{inparaenum}

We organize the remainder of our paper as follows.
Section~\ref{sec_technical} presents some technical background.
Section~\ref{sec_problem} shows the issues with existing Linux mechanisms.
Section \ref{sec_solution} presents MemOpLight.
Section~\ref{sec_evaluation} compares its performance with existing Linux mechanisms.
In Section~\ref{sec_related_works}, we review related work.
Finally, we conclude and discuss future work in Section~\ref{sec_conclusion}.