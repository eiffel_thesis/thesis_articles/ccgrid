\section{Memory optimization light (MemOpLight)}
\label{sec_solution}
It is difficult to do better with the static limits and the limited information available to the kernel.
The kernel only has access to low level metrics such as the CPU cycles used or the I/O bandwidth.
Theses are, at best, proxies for application level quality of service, and are not representative of the needs of modern applications.

Instead, we propose to base memory reclamation on performance metrics perceived by the application.
The container itself declares its current level of satisfaction, according to its own, application-specific metrics (\textit{e.g.} latency for a web server, frame rate for a streaming application, etc.), compared to some performance objectives.
We associate a traffic light color to different states of performance:
\begin{itemize}
	\item GREEN: The container has reached its maximum level and would not benefit from more resources.
	For instance, a web server has no requests waiting.
	\item YELLOW: The container is satisfied, but it would do better with more resources.
	For example, a web server has requests waiting in a queue but answered requests were dealt under a given latency.
	\item RED: The container is not satisfied, \textit{e.g.} a web server is not able to answer requests under a certain latency.
\end{itemize}

\begin{figure*} % Algorithm.
	\centering

	\begin{algorithmic}[1]
		\Function{$reclaim\_cgroup\_list$}{$cgroups$}
			\State $reclaimed = 0$
			\State $expected = 0$
			\ForAll{$iter \in cgroups$}
				\If{$\neg iter.reclaimed$} \Comment{If \texttt{cgroup} was not reclaimed in the current time slot.}
					\State $to\_take = \frac{mem\_cgroup\_usage(iter, false)}{iter.divider}$ \Comment{Take away some amount of current \texttt{cgroup} memory.}
					\State $local\_reclaimed = try\_to\_free\_mem\_cgroup\_pages(iter, to\_take)$ \Comment{Try to free this amount.}
					\State $reclaimed \mathrel{+}= local\_reclaimed$
					\State $expected \mathrel{+}= to\_take$
					\State $iter.reclaimed = true$ \Comment{Mark this \texttt{cgroup} as already reclaimed until next time slot. The probe will reset this flag in next time slot.}
				\EndIf
			\EndFor
			\State \Return $(reclaimed, expected)$
		\EndFunction

		\Function{$mem\_op\_light$}{}
			\State $reds = 0$
			\State $yellows = []$
			\State $greens = []$

			\Comment{This loop will count the number of red containers and add the others to corresponding list.}
			\ForAll{$iter \in mem\_cgroups$} \Comment {For all \texttt{cgroups} in the system.}
				\If{$iter.color == RED$} \Comment{If the \texttt{cgroup} has bad performance.}
					\State $reds++$
				\ElsIf{$iter.color == YELLOW$} \Comment{If the \texttt{cgroup} respects its SLO but can do more.}
					\State $yellows.append(iter)$
				\ElsIf{$iter.color == GREEN$} \Comment{If the \texttt{cgroup} has best performance possible.}
					\State $greens.append(iter)$
				\EndIf
			\EndFor

			\State $reclaimed = 0$
			\State $expected = 0$

			\Comment{We will now reclaim memory}
			\If{$reds \lor len(yellows)$} \Comment{If there is at least one red or one yellow, the green \texttt{cgroups} will be reclaimed.}
				\State $green\_reclaimed, green\_expected = $ \Call{$reclaim\_cgroup\_list$}{$greens$}
				\State $reclaimed \mathrel{+}= green\_reclaimed$
				\State $expected \mathrel{+}= green\_expected$
			\EndIf

			\If{$reds$} \Comment{If there is at least one red we also reclaim the yellow \texttt{cgroups}.}
				\State $yellow\_reclaimed, yellow\_expected = $ \Call{$reclaim\_cgroup\_list$}{$yellows$}
				\State $reclaimed \mathrel{+}= yellow\_reclaimed$
				\State $expected \mathrel{+}= yellow\_expected$
			\EndIf

			\If{$expected \land reclaimed \geq expected$} \Comment{Quit if enough memory reclaimed.}
				\State \Return $reclaimed$
			\EndIf
			\State \Return $0$ \Comment{Quit and indicates that memory reclaim failed.}
		\EndFunction
	\end{algorithmic}

	\caption{MemOpLight algorithm}
	\label{lst_memoplight}
\end{figure*}

Our approach is based on two components:
\begin{enumerate}
	\item A container is equipped with an applicative probe, which indicates its color, based on some application-specific objective.
	In our prototype, the probe compares the transaction latency of \texttt{mysql} to some SLO and informs Linux through \texttt{sysfs} every second.
	Specifically, a container with high throughput declares itself green if its throughput equals the SLO and has no transactions waiting.
	With low throughput, it is green if it handles $\approx 200$ t/s.
	If it cannot respect its SLO, it is red.
	Otherwise, if it has transactions waiting, it is yellow.
	\item An algorithm that executes when memory is scarce.
	As described in Figure \ref{lst_memoplight}, MemOpLight reclaims some fraction of the physical memory footprint from first green then yellow containers.
	We set this proportion to 2\% to smooth the transitions from green/yellow to red.
	Indeed, if a container declares itself as green or yellow, this must mean that its WS fits into its current physical memory allocation.
	MemOpLight avoids reclaiming when container is red.
	This mechanism tends to adapt the amount of memory to what is required for each container to be satisfied.
	Memory reclaimed can be used by other containers to improve their own performance and satisfaction.
\end{enumerate}

\begin{figure*} % Algorithm.
	\centering

	\begin{algorithmic}[1]
		\State $mem\_op\_light\_lock = init\_lock()$

		\Function{$timer$}{}
			\State $unlock(mem\_op\_light\_lock)$
		\EndFunction

		\Function{$mem\_cgroup\_soft\_limit\_reclaim$}{}
			\If{$lock(mem\_op\_light\_lock)$} \Comment{If lock is free MemOpLight is not executing or was called more than a second ago.}
				\State $reclaimed = mem\_op\_light()$
				\If{$reclaimed$}
					\State $set\_timer(timer, 1)$ \Comment{Install a timer to fire in 1 second.}
					\State \Return $reclaimed$ \Comment{If we reclaimed enough memory we quit now and guarantee that MemOpLight and \texttt{soft} limit will not be called before one second.}
				\EndIf
				\State $unlock(mem\_op\_light\_lock)$
			\Else
				\State \Return $0$ \Comment{Either MemOpLight is already executing or it was called during this second.}
			\EndIf
			\Comment{Otherwise continue into \texttt{soft} limit mechanism.}
		\EndFunction
	\end{algorithmic}

	\caption{\texttt{mem\_cgroup\_soft\_limit} modification algorithm}
	\label{lst_mem_cgroup_soft_limit}
\end{figure*}

We implemented MemOpLight in existing Linux kernel code.
Figure \ref{lst_mem_cgroup_soft_limit} shows our modifications to kernel function \texttt{mem\_cgroup\_soft\_limit\_reclaim}.
It calls MemOpLight once per second when there is memory pressure.
If MemOpLight fails to reclaim memory, the \texttt{soft} limit mechanism activates.
Our modifications amount to $\approx 400$ lines of code.